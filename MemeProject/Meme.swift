//
//  ViewController.swift
//  MemeProject
//
//  Created by Qutaibah Essa on 27/11/2018.
//  Copyright © 2018 qutaibah. All rights reserved.
//

import UIKit

struct Meme {
	let topText: String
	let bottomText: String
	let originalImage: UIImage
	let memedImage: UIImage
}
