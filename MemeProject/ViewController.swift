//
//  ViewController.swift
//  MemeProject
//
//  Created by Qutaibah Essa on 27/11/2018.
//  Copyright © 2018 qutaibah. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var btnCamera: UIBarButtonItem!
	@IBOutlet weak var txtTop: UITextField!
	@IBOutlet weak var txtBottom: UITextField!
	@IBOutlet weak var topToolbar: UIToolbar!
	@IBOutlet weak var bottomToolbar: UIToolbar!

	override func viewDidLoad() {
		super.viewDidLoad()
		setTextFields(textInput: txtTop, defaultText: "TOP")
		setTextFields(textInput: txtBottom, defaultText: "BOTTOM")
		btnCamera.isEnabled = UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		subscribeToKeyboardNotifications()
	}

	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		unSubscribeToKeyboardNotofications()
	}

	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		view.endEditing(true)
	}

	@IBAction func showLibrary(_ sender: AnyObject) {
		showImagePicker(with: .photoLibrary)
	}

	@IBAction func showCamera(_ sender: AnyObject) {
		showImagePicker(with: .camera)
	}

	func showImagePicker(with source: UIImagePickerController.SourceType){
		let pickerController = UIImagePickerController()
		pickerController.delegate = self
		pickerController.sourceType = source
		present(pickerController, animated: true, completion: nil)
	}

	@IBAction func share(_ sender: AnyObject) {
		let ac = UIActivityViewController(activityItems: [generateMemedImage()],
										  applicationActivities: nil)
		ac.completionWithItemsHandler = { _, success, _, _ in
			if success {
				self.save()
			}
		}
		present(ac, animated: true, completion: nil)
	}

	func generateMemedImage() -> UIImage {
		hideToolbar(true)
		UIGraphicsBeginImageContext(view.frame.size)
		view.drawHierarchy(in: view.frame, afterScreenUpdates: true)
		let memedImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
		UIGraphicsEndImageContext()
		hideToolbar(false)
		return memedImage
	}

	func save() {
		_ = Meme(topText: txtTop.text!, bottomText: txtBottom.text!, originalImage: imageView.image!, memedImage: generateMemedImage())
	}

	func setTextFields(textInput: UITextField!, defaultText: String) {
		let memeTextAttributes: [NSAttributedString.Key: Any] = [
			NSAttributedString.Key.strokeColor : UIColor.black,
			NSAttributedString.Key.foregroundColor : UIColor.white,
			NSAttributedString.Key.font : UIFont(name: "HelveticaNeue-CondensedBlack", size: 40)!,
			NSAttributedString.Key.strokeWidth : -3.0]
		textInput.text = defaultText
		textInput.defaultTextAttributes = memeTextAttributes
		textInput.delegate = self
		textInput.textAlignment = .center
	}

	func hideToolbar(_ hide: Bool) {
		topToolbar.isHidden = hide
		bottomToolbar.isHidden = hide
	}

	func subscribeToKeyboardNotifications() {
		NotificationCenter.default.addObserver(self,
											   selector: #selector(keyboardWillShow(_:)),
											   name: UIResponder.keyboardWillShowNotification,
											   object: nil)
		NotificationCenter.default.addObserver(self,
											   selector: #selector(keyboardWillHide(_:)),
											   name: UIResponder.keyboardWillHideNotification,
											   object: nil)
	}

	func unSubscribeToKeyboardNotofications() {
		NotificationCenter.default.removeObserver(self,
												  name: UIResponder.keyboardWillShowNotification,
												  object: nil)
		NotificationCenter.default.removeObserver(self,
												  name: UIResponder.keyboardWillHideNotification,
												  object: nil)
	}

	@objc func keyboardWillShow(_ notification: NSNotification) {
		if txtBottom.isFirstResponder && view.frame.origin.y == 0 {
			view.frame.origin.y -= getKeyboardHeight(notification)
		}
	}

	@objc func keyboardWillHide(_ notification: NSNotification) {
		view.frame.origin.y = 0
	}

	func getKeyboardHeight(_ notification: NSNotification) -> CGFloat {
		let userInfo = notification.userInfo
		let keyboardSize = userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
		return keyboardSize.cgRectValue.height
	}
}

extension ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {

	func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
		dismiss(animated: true, completion: nil)
	}

	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
		let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage ?? info[UIImagePickerController.InfoKey.originalImage] as? UIImage
		imageView.image = image
		dismiss(animated: true, completion: nil)
	}
}

extension ViewController: UITextFieldDelegate {

	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		if txtTop.isFirstResponder {
			txtBottom.becomeFirstResponder()
		} else {
			view.endEditing(true)
		}
		return true
	}

	func textFieldDidBeginEditing(_ textField: UITextField) {
		if textField == txtTop && textField.text == "TOP" {
			textField.text = ""
		}
		if textField == txtBottom && textField.text == "BOTTOM" {
			textField.text = ""
		}
	}

	func textFieldDidEndEditing(_ textField: UITextField) {
		if txtTop.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == "" {
			txtTop.text = "TOP"
		}
		if txtBottom.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == "" {
			txtBottom.text = "BOTTOM"
		}
	}
}
